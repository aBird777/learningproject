package com.luv2code.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {
		String jdbcUrl = "jdbc:postgresql://localhost:5432/hibernate-one-to-one";
		String user = "postgres";
		String pass = "admin";
		Connection con = null;
		try
		{
			
			System.out.println("connection to database:"+jdbcUrl);
			con = DriverManager.getConnection(jdbcUrl,user,pass);
			System.out.println("connection successful!");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		

	}

}
