package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateInstructorDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration()
									.configure("hibernate.cfg.xml")
									.addAnnotatedClass(Instructor.class)
									.addAnnotatedClass(InstructorDetail.class)
									.addAnnotatedClass(Course.class)
									.buildSessionFactory();
								
		//create session
		Session session = factory.getCurrentSession();
		try
		{
			//create the objects
			Instructor instructor = 
					new Instructor("suzan","public","suzanPublic@luv2code.com");
			
			InstructorDetail instructorDetail = 
					new InstructorDetail("http://www.luv2code.com/youtube",
											"love to game!!");
			//associate the objects together
			
			instructor.setInstructorDetail(instructorDetail);
		
			//begin transaction
			session.beginTransaction();
			
			//save the instructor
			
			System.out.println("Saving the intructor: "+instructor);
			
			session.save(instructor);
			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			
		}
		finally {
			session.close();
			factory.close();
		}
	}
}
