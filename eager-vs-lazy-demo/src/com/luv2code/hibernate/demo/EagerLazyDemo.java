package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class EagerLazyDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration()
									.configure("hibernate.cfg.xml")
									.addAnnotatedClass(Instructor.class)
									.addAnnotatedClass(InstructorDetail.class)
									.addAnnotatedClass(Course.class)
									.buildSessionFactory();
								
		//create session
		Session session = factory.getCurrentSession();
		try
		{
			
			//begin transaction
			session.beginTransaction();
			
			Instructor instructor = session.get(Instructor.class, 1);
			
			//print instructor 
			System.out.println("luv2code: Instructor : "+instructor);
			
			//commit transaction
			session.getTransaction().commit();
			

			session.close();
			factory.close();
			
			//print instructor courses
			System.out.println("luv2code: Instructor course : "+instructor.getCourses());
			
			//System.out.println("luv2code: Done!");
			
			
		}
		finally {
			
		}
	}
}
