package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class CreateCoursesDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration()
									.configure("hibernate.cfg.xml")
									.addAnnotatedClass(Instructor.class)
									.addAnnotatedClass(InstructorDetail.class)
									.addAnnotatedClass(Course.class)
									.buildSessionFactory();
								
		//create session
		Session session = factory.getCurrentSession();
		try
		{
			
			//begin transaction
			session.beginTransaction();
			
			//get the instructor from db
			Instructor instructor = session.get(Instructor.class, 1);
			//create courses
			Course course1 = new Course("Air Guitar123");
			Course course2 = new Course("Pin Ball Master class");
			//add some courses to the instructor
			instructor.add(course1);
			instructor.add(course2);
			//save courses
			session.save(course1);
			session.save(course2);
			
			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			
		}
		finally {
			session.close();
			factory.close();
		}
	}
}
