package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class DeleteInstructorDetailAndNotInstructorDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration()
									.configure("hibernate.cfg.xml")
									.addAnnotatedClass(Instructor.class)
									.addAnnotatedClass(InstructorDetail.class)
									.buildSessionFactory();
								
		//create session
		Session session = factory.getCurrentSession();
		try
		{
			int id = 5;
			//begin transaction
			session.beginTransaction();
			
			//get the instructorDetail
			InstructorDetail instructorDetail = session.get(InstructorDetail.class,id );
			
			//set instructor field to null
			//instructorDetail.getInstructor().setInstructorDetail(null);
			
			
			if(instructorDetail != null)
			{
				System.out.println("Deleting the the intructor Detail: "+instructorDetail);
				session.delete(instructorDetail);
			}
			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			
		}
		finally {
			session.close();
		}
	}
}
