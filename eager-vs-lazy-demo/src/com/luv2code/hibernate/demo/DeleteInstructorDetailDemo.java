package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class DeleteInstructorDetailDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration()
									.configure("hibernate.cfg.xml")
									.addAnnotatedClass(Instructor.class)
									.addAnnotatedClass(InstructorDetail.class)
									.buildSessionFactory();
								
		//create session
		Session session = factory.getCurrentSession();
		try
		{
			int id = 1;
			//begin transaction
			session.beginTransaction();
			
			//get the instructor
			Instructor instructor = session.get(Instructor.class,id );
			
			if(instructor != null)
			{
				System.out.println("Deleting the the intructor: "+instructor);
				session.delete(instructor);
			}
			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			
		}
		finally {
			session.close();
		}
	}
}
